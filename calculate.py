#!/usr/bin/python2.7
"""
Description:
Created by : Seddik Alaoui Ismaili & farouk mansour
Script was created for calculate ip subnet
Ip address, subnet mask, network address, host ranges, and broadcast

Usage :
./calculate.py 10.10.0.1/25
"""

## get import modules
import argparse, sys
"""
parser = argparse.ArgumentParser(description='Calculate Subnet ....')
parser.add_argument('-f', '--file', action="store", nargs='?')
args = parser.parse_args()


if args.file:
    print("the name of file is {}".format(args.file))
else:
    print("Fatal ERROR")
    sys.exit(2)
"""

## funtion to convert into binary
def _to_binary(ip_address):
    return map(lambda x: bin(x)[2:].zfill(8), ip_address)


## het netmask
def _nega_mask(net_mask):
    mask = list()
    for i in net_mask:
        mask.append(255 - int(i))
    return mask


class IPCaculate(object):
    def __init__(self, ip_address, cdir=24):
        if '/' in ip_address:
            self._address_val, self._cidr = ip_address.split('/')
            self._address = map(int, self._address_val.split('.'))
        else:
            self._address = map(int, ip_address.split('.'))
            self._cidr = cdir
        self.binary_IP = _to_binary(self._address)
        self.binary_Mask = None
        self.negation_Mask = None
        self.network = None
        self.broadcast = None

    def __repr__(self):
        print "Calculating the IP range of %s/%s" % (".".join(map(str, self._address)), self._cidr)
        print "#####################################################"
        print "Netmask %s" % (".".join(map(str, self.net_mask())))
        print "Network Addr %s" % (".".join(map(str, self.network_ip())))
        print "Subnet Broadcast address %s" % (".".join(map(str, self.broadcast_ip())))
        print "Host range %s" % (self.host_range())
        print "Max number of hosts %s" % (self.number_of_host())

    def net_mask(self):
        mask = [0, 0, 0, 0]
        for i in range(int(self._cidr)):
            mask[i / 8] += 1 << (7 - i % 8)
        self.binary_Mask = _to_binary(mask)
        self.negation_Mask = _to_binary(_nega_mask(mask))
        return mask

    def broadcast_ip(self):
        broadcast = list()
        for x, y in zip(self.binary_IP, self.negation_Mask):
            broadcast.append(int(x, 2) | int(y, 2))
        self.broadcast = broadcast
        return broadcast

    def network_ip(self):
        network = list()
        for x, y in zip(self.binary_IP, self.binary_Mask):
            network.append(int(x, 2) & int(y, 2))
        self.network = network
        return network

    def host_range(self):
        min_range = self.network
        min_range[-1] += 1
        max_range = self.broadcast
        max_range[-1] -= 1
        return "%s - %s" % (".".join(map(str, min_range)), ".".join(map(str, max_range)))

    def number_of_host(self):
        return (2 ** sum(map(lambda x: sum(c == '1' for c in x), self.negation_Mask))) - 2

    def ip_calculate(ip):
        ip = IPCaculate(ip)
        ip.__repr__()

    ## check after this if argument is given
    if __name__ == '__main__':
        if len(sys.argv) > 1:
            p = sys.argv[1]
            ip_calculate(ip)
        else:
            sys.exit(0)






